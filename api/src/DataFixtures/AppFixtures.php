<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\User;
use App\Entity\Property;
use App\Entity\Booking;

use Faker;

class AppFixtures extends Fixture
{
  private $faker;

  public function __construct()
  {
    $this->faker = \Faker\Factory::create('fr_FR');
  }

  public function load(ObjectManager $manager)
  {
    $this->createAdmin($manager);
    $this->createUsersFixtures($manager, 150);
    $this->createCampingMobilehomesFixtures($manager, 20, 'contact@camping.espadrille.volante');
    $this->createPrivateMobilehomesFixtures($manager, 30, $manager->getRepository(User::class)->findAll());
    $this->createCaravanFixtures($manager, 10, $manager->getRepository(User::class)->findAll());
    $this->createLocationFixtures($manager, 30, $manager->getRepository(User::class)->findAll());
  }

  private function createAdmin(ObjectManager $manager)
  {
    $adminFixture = new User();

    $adminFixture
      ->setUserImageUrl("https://images-na.ssl-images-amazon.com/images/I/41dgHMegc0L.jpg")
      ->setRoles(["ROLE_ADMIN"])
      ->setEmail("contact@camping.espadrille.volante")
      ->setPassword("admin")
      ->setSurname("Camping Espadrille Volante")
      ->setName("ADMIN");

    $manager->persist($adminFixture);
    $manager->flush();
  }

  private function createUsersFixtures(ObjectManager $manager, int $count)
  {
    for ($i = 0; $i < $count; $i++) {
      $userFixture = new User();

      $userFixtureGender = rand(0, 1);
      $userFixtureAvatarId = rand(0, 99);
      $userFixtureLastName = $this->faker->lastName;

      if ($userFixtureGender === 0) {
        $userFixtureFirstName = ($this->faker->firstNameMale);

        $userFixture->setUserImageUrl('https://randomuser.me/api/portraits/men/' . $userFixtureAvatarId . '.jpg');
      } else {
        $userFixtureFirstName = ($this->faker->firstNameFemale);

        $userFixture->setUserImageUrl('https://randomuser.me/api/portraits/women/' . $userFixtureAvatarId . '.jpg');
      }

      $userFixture
        ->setRoles(['ROLE_USER'])
        ->setEmail($this->mailifyString($userFixtureFirstName . "." . $userFixtureLastName . "@" . $this->faker->freeEmailDomain))
        ->setPassword("user")
        ->setSurname($userFixtureLastName)
        ->setName($userFixtureFirstName);

      $manager->persist($userFixture);
      $manager->flush();
    }
  }

  private function createCampingMobilehomesFixtures(ObjectManager $manager, int $count, $campingMail)
  {
    $owner = $manager->getRepository(User::class)->findOneBy(['email' => $campingMail]);

    for ($i = 0; $i < $count; $i++) {
      $propertyFixture = new Property();

      $propertyFixture
        ->setType(0)
        ->setName($this->faker->sentence(rand(2, 4), true))
        ->setMaxCapacity(rand(2, 4))
        ->setPrice(rand(20, 100))
        ->setOwner($owner)
        ->setPropertyImageUrl($this->getUnsplashImage("caravan"));

      $manager->persist($propertyFixture);
      $manager->flush();
    }
  }

  private function createPrivateMobilehomesFixtures(ObjectManager $manager, int $count, $owners)
  {
    for ($i = 0; $i < $count; $i++) {
      $propertyFixture = new Property();
      $owner = $owners[rand(0, count($owners) - 1)];

      $propertyFixture
        ->setType(0)
        ->setName($this->faker->sentence(rand(2, 4), true))
        ->setMaxCapacity(rand(2, 4))
        ->setPrice(rand(20, 100))
        ->setOwner($owner)
        ->setPropertyImageUrl($this->getUnsplashImage("caravan"));

      $manager->persist($propertyFixture);
      $manager->flush();
    }
  }

  private function createCaravanFixtures(ObjectManager $manager, int $count, $owners)
  {
    for ($i = 0; $i < $count; $i++) {
      $propertyFixture = new Property();
      $owner = $owners[rand(0, count($owners) - 1)];

      $propertyFixture
        ->setType(1)
        ->setName($this->faker->sentence(rand(2, 4), true))
        ->setMaxCapacity(rand(2, 4))
        ->setPrice(rand(20, 100))
        ->setOwner($owner)
        ->setPropertyImageUrl($this->getUnsplashImage("caravan"));

      $manager->persist($propertyFixture);
      $manager->flush();
    }
  }

  private function createLocationFixtures(ObjectManager $manager, int $count, $owners)
  {
    for ($i = 0; $i < $count; $i++) {
      $propertyFixture = new Property();
      $owner = $owners[rand(0, count($owners) - 1)];

      $propertyFixture
        ->setType(2)
        ->setName($this->faker->sentence(rand(3, 6), true))
        ->setMaxCapacity(rand(2, 4))
        ->setPrice(rand(20, 100))
        ->setOwner($owner)
        ->setPropertyImageUrl($this->getUnsplashImage("caravan+location"));

      $manager->persist($propertyFixture);
      $manager->flush();
    }
  }

  private function getUnsplashImage($keyword): ?string
  {
    $url = "https://source.unsplash.com/featured/?" . $keyword;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_exec($ch);

    $redirectedUrl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);

    return $redirectedUrl;
  }

  private function mailifyString($string)
  {
    $unwanted_array = [
      'Š' => 'S', 'š' => 's', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
      'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U',
      'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c',
      'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o',
      'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y', 'Ğ' => 'G', 'İ' => 'I', 'Ş' => 'S', 'ğ' => 'g', 'ı' => 'i', 'ş' => 's',
      'ü' => 'u', 'ă' => 'a', 'Ă' => 'A', 'ș' => 's', 'Ș' => 'S', 'ț' => 't', 'Ț' => 'T', ' ' => '-'
    ];

    $newString = strtolower(strtr($string, $unwanted_array));

    return $newString;
  }
}
