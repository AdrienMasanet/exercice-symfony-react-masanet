import React, { Fragment } from "react";

const Contact = () =>
{
  return (
    <Fragment>
      <h1 className="text-center">N'hésitez pas à nous contacter !</h1>
      <div className="h-100 d-flex justify-content-center align-items-center flex-grow-1">
        <h3>- ici le contact form -</h3>
      </div>
    </Fragment>
  );

}

export default Contact;