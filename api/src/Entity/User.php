<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ApiResource
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
  /**
   * @ORM\Id
   * @ORM\GeneratedValue
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=180, unique=true)
   */
  private $email;

  /**
   * @ORM\Column(type="json")
   */
  private $roles = [];

  /**
   * @var string The hashed password
   * @ORM\Column(type="string")
   */
  private $password;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $name;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $surname;

  /**
   * @ORM\OneToMany(targetEntity=Property::class, mappedBy="owner", orphanRemoval=true)
   */
  private $properties;

  /**
   * @ORM\OneToMany(targetEntity=Booking::class, mappedBy="user")
   */
  private $bookings;

  /**
   * @ORM\Column(type="text", nullable=true)
   */
  private $userImageUrl;

  public function __construct()
  {
    $this->properties = new ArrayCollection();
    $this->bookings = new ArrayCollection();
  }

  public function getId(): ?int
  {
    return $this->id;
  }

  public function getUserIdentifier(): string
  {
    return (string) $this->email;
  }

  public function getEmail(): ?string
  {
    return $this->email;
  }

  public function setEmail(string $email): self
  {
    $this->email = $email;

    return $this;
  }

  public function getUsername()
  {
    
  }

  public function getRoles(): array
  {
    $roles = $this->roles;
    // guarantee every user at least has ROLE_USER
    $roles[] = 'ROLE_USER';

    return array_unique($roles);
  }

  public function setRoles(array $roles): self
  {
    $this->roles = $roles;

    return $this;
  }

  /**
   * @see PasswordAuthenticatedUserInterface
   */
  public function getPassword(): string
  {
    return $this->password;
  }

  public function setPassword(string $password): self
  {
    $this->password = $password;

    return $this;
  }

  public function getSalt(): ?string
  {
    return null;
  }

  public function eraseCredentials()
  {
    
  }

  public function getName(): ?string
  {
    return $this->name;
  }

  public function setName(string $name): self
  {
    $this->name = $name;

    return $this;
  }

  public function getSurname(): ?string
  {
    return $this->surname;
  }

  public function setSurname(string $surname): self
  {
    $this->surname = $surname;

    return $this;
  }

  /**
   * @return Collection|Property[]
   */
  public function getProperties(): Collection
  {
    return $this->properties;
  }

  public function addProperty(Property $property): self
  {
    if (!$this->properties->contains($property)) {
      $this->properties[] = $property;
      $property->setOwner($this);
    }

    return $this;
  }

  public function removeProperty(Property $property): self
  {
    if ($this->properties->removeElement($property)) {
      // set the owning side to null (unless already changed)
      if ($property->getOwner() === $this) {
        $property->setOwner(null);
      }
    }

    return $this;
  }

  /**
   * @return Collection|Booking[]
   */
  public function getBookings(): Collection
  {
    return $this->bookings;
  }

  public function addBooking(Booking $booking): self
  {
    if (!$this->bookings->contains($booking)) {
      $this->bookings[] = $booking;
      $booking->setUser($this);
    }

    return $this;
  }

  public function removeBooking(Booking $booking): self
  {
    if ($this->bookings->removeElement($booking)) {
      // set the owning side to null (unless already changed)
      if ($booking->getUser() === $this) {
        $booking->setUser(null);
      }
    }

    return $this;
  }

  public function getUserImageUrl(): ?string
  {
    return $this->userImageUrl;
  }

  public function setUserImageUrl(?string $userImageUrl): self
  {
    $this->userImageUrl = $userImageUrl;

    return $this;
  }
}
