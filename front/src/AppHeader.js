import React from "react";
import { Link } from "react-router-dom";

const AppHeader = () =>
{

  return (
    <section className="w-100 h-auto mt-auto mb-4 px-4 p-2 d-flex justify-content-between align-items-center" style={ { background: "rgba(0,0,0,.1)" } }>
      <Link className="menu-item mx-1 p-2" to="/">
        <h4 className="text-center">Camping<br />L'espadrille volante</h4>
      </Link>
      <nav>
        <div className="d-flex list-unstyled m-0">
          <Link className="menu-item mx-1 p-2" to="/">Accueil</Link>
          <Link className="menu-item mx-1 p-2" to="/louer">Louer</Link>
          <Link className="menu-item mx-1 p-2" to="/tarifs">Tarifs</Link>
          <Link className="menu-item mx-1 p-2" to="/actualites">Actualités</Link>
          <Link className="menu-item mx-1 p-2" to="/contact">Contact</Link>
        </div>
      </nav>

      <div className="text-center small">
        <a className="menu-item list-unstyled mx-1 p-2" href="/connexion">Se connecter</a>
        <hr className="my-1" />
        <a className="menu-item list-unstyled mx-1 p-2" href="/creer-compte">Créer un compte</a>
      </div>
    </section>
  );

}

export default AppHeader;
