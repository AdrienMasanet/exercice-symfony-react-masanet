import React, { Fragment } from "react";

const Error404 = () =>
{
  return (
    <Fragment>
      <h1 className="text-center">Erreur 404</h1>
      <div className="h-100 d-flex justify-content-center align-items-center flex-grow-1">
        <h3>La page que vous avez demandée n'existe pas</h3>
      </div>
    </Fragment>
  );

}

export default Error404;