import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import AppHeader from "./AppHeader";

import Home from "./pages/Home";
import Properties from "./pages/Properties";
import Contact from "./pages/Contact";
import Prices from "./pages/Prices";
import Blog from "./pages/Blog";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Error404 from "./pages/Error404";

import AppFooter from "./AppFooter";

const App = () =>
{

  // On assigne la taille minimale de l'élément root à la taille actuelle de la fenêtre du navigateur
  document.querySelector( "#root" ).style.minHeight = window.innerHeight + "px";

  React.useEffect( () =>
  {
    window.addEventListener( 'resize', ( event ) =>
    {
      document.querySelector( "#root" ).style.minHeight = window.innerHeight + "px";
    } );
  }, [] );

  return (
    <BrowserRouter>

      <AppHeader />

      <div className="container flex-grow-1 d-flex flex-column">
        <Switch>
          <Route exact path="/" component={ Home } />"
          <Route path="/louer" component={ Properties } />
          <Route path="/contact" component={ Contact } />
          <Route path="/tarifs" component={ Prices } />
          <Route path="/actualites" component={ Blog } />
          <Route path="/connexion" component={ Login } />
          <Route path="/creer-compte" component={ Register } />
          <Route component={ Error404 } />
        </Switch>
      </div>

      <AppFooter />

    </BrowserRouter>
  );

}

ReactDOM.render( <App />, document.querySelector( "#root" ) );
