import React, { Fragment } from "react";

const Blog = () =>
{
  return (
    <Fragment>
      <h1 className="text-center">Actualités</h1>
      <div className="h-100 d-flex justify-content-center align-items-center flex-grow-1">
        <h3>- ici la liste dynamique des actualités -</h3>
      </div>
    </Fragment>
  );

}

export default Blog;