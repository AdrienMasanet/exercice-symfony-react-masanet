import React, { Component, Fragment } from "react";
import axios from "axios";

class BrowseLocations extends Component
{
  state = {
    properties: []
  }

  getProperties = async () =>
  {
    console.log( "Getting properties..." );

    await axios.get( process.env.REACT_APP_APIURL + "properties" )
      .then( ( response ) =>
      {
        console.log( response.data[ "hydra:member" ] );
        this.setState( { properties: response.data[ "hydra:member" ] } );
      } )
      .catch( ( error ) =>
      {
        console.log( "Cannot reach and fetch API. Error : " + error );
      } );

  }

  componentDidMount ()
  {
    this.getProperties();
  }

  render ()
  {
    if ( this.state.properties.length ) {
      return (
        <Fragment>
          <h1 className="text-center">Biens disponibles à la location</h1>
          <div className="d-flex flex-wrap justify-content-center align-items-stretch text-center">
            { this.state.properties.map( property => (
              <div key={ "property" + property.id } className="card m-1" id={ "property_" + property.id } style={ { width: "20%" } }>
                <img className="card-img-top h-50" style={ { objectFit: "cover" } } src={ property.propertyImageUrl } alt={ "Photo du bien à louer n°" + property.id } />
                <div className="card-body">
                  <div className="card-title">
                    <h5>{ property.name }</h5>
                  </div>
                </div>
              </div>
            ) ) }
          </div>
        </Fragment>
      );
    } else {
      return (
        <Fragment>
          <h1 className="text-center">&nbsp;&nbsp;Chargement...</h1>
          <div className="h-100 d-flex justify-content-center align-items-center flex-grow-1">
            <img src={ process.env.PUBLIC_URL + '/spinner.gif' } alt="chargement" />
          </div>
        </Fragment>
      );
    }
  }

}

export default BrowseLocations;